/*
Stand 11.06.2021:
WWS-4506 Performanceverbesserung

Stand: 15.01.2021

*/

/* WWS-4506 Erweiterung    +1 Zeile  */
var orderGuid as Guid;
var order as String;
var status as String;
var orderType as String;
var orderStatus as String;

function create()
{
/* set job queue and user for batch job */
setJobQueue("JQOT");
setJobUser("ADMINISTRATOR");

/* WWS-4506 Erweiterung    +1 Zeile  */
orderGuid:=parameters.object:guid;
order:=parameters.object:number;
status:="ORDER_PENDING";

/*Ignore wrong ordertypes or multiple events ---------------*/

orderType:=parameters.newObject->Type:code;
orderStatus:=status + " " + order;

if (cast(Number, getResultList("SELECT count(*) FROM com.cisag.sys.workflow.obj.Activity A WHERE A:description = ?", list(orderStatus))[0][0]) > 0 OR (orderType <> "105" AND orderType <> "104") OR startsWith(order, "VA") = true)
{
abort();
}


/*Call osCheck:---------------*/

formatSubject("Beschreibung", status + " " + order);
/* WWS-4506 Erweiterung    +1 Zeile  */
call("osCheck", hashMap(ORDER:=order, STATUS:=status, MODUS:="complete", GUID:=orderGuid));
}

