var beschreibung as String;
var url as String;
var inhalt as String;

function create()
{
/* set job queue and user for batch job */
setJobQueue("JQOT");
setJobUser("ADMINISTRATOR");

url:=cast(String, parameters.parameters.URL);
beschreibung:=cast(String, parameters.parameters.BESCHREIBUNG);
inhalt:=cast(String, parameters.parameters.INHALT);
formatSubject("Beschreibung", beschreibung);  /*Testmodus für Parallelbetrieb mit altem Order-Status durch "Test"-Prefix*/
fireEvent("osTransferCheck", hashMap(ORDERSTATUS:=beschreibung));
}
