/*
Stand 11.06.2021:
WWS-4506 Performanceverbesserung

Stand: 22.04.2021
WWS-4451 Erweiterung die SST OrderService um "originator"

*/

var status as String;
var orderType as String;
var orderStatus as String;
var orderGuid as Guid;
var order as String;
var tourNumber as String;
var rogNumbers as String;
/* Erweiterung um OQL für WWS-4451  BEGIN */
var originator as String;
/* Erweiterung um OQL für WWS-4451  ENDE */

function create()
{
/* set job queue and user for batch job */
setJobQueue("JQOT");
setJobUser("ADMINISTRATOR");

orderGuid:=cast(Guid, getResultList("SELECT SO:guid FROM com.cisag.app.sales.obj.SalesOrder SO JOIN com.cisag.app.purchasing.obj.ReceiptOfGoodsOrderInfo ROGI ON ROGI:\"order\" = SO:guid WHERE ROGI:header = ?", list(parameters.object:guid),1)[0][0]);

var orderObj:=getByPrimaryKey(CisObject(com.cisag.app.sales.obj.SalesOrder), orderGuid);
var tour as CisObject(com.bring.app.sales.obj.Tour);
tour:= orderObj->Bring_Tour;

/* Erweiterung um OQL für WWS-4451  BEGIN */
originator:=getByPrimaryKey(CisObject(com.cisag.app.general.obj.Partner), getByPrimaryKey(CisObject(com.cisag.app.general.obj.UserAssignment), parameters.object:updateInfo.createUser):partner):number;
/* Erweiterung um OQL für WWS-4451  ENDE */

status:="PARTIAL_CANCELLATION_UPDATED";
order:=orderObj:number;
tourNumber:=tour:number;
var getRogs:=call("getRogs", hashMap(ORDER:=orderGuid));
rogNumbers:=cast(String, getRogs.ROGS);

/*Ignore wrong ordertypes or multiple events ---------------*/

orderStatus:=status + " " + order + " " + tourNumber + " " + rogNumbers;
orderType:=orderObj->Type:code;

/*echo(orderStatus);*/

if (cast(Number, getResultList("SELECT count(*) FROM com.cisag.sys.workflow.obj.Activity A WHERE A:description = ?", list(orderStatus))[0][0]) > 0 OR (orderType <> "105" AND orderType <> "104") OR startsWith(order, "VA") = true)
{
abort();
}

formatSubject("Beschreibung", orderStatus);                                                 
/* WWS-4506 Anpassung    +1 Zeile  */
call("osCheck", hashMap(ORDER:=order, STATUS:=status, MODUS:="complete", TOUR:=tourNumber, ROGS:=rogNumbers, ORIGINATOR:=originator, GUID:=orderGuid));

}
