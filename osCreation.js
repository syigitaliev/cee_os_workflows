/*
Stand 18.10.2021
SRV-17935  KG Artikel wurde in Stk bestellt und Kg ausgeliefert  +1 Zeile

Stand 01.07.2021:
SRV-17909 Anpassung der Übergabe von deliveredUnit

Stand 01.07.2021:
WWS-4516 Anpassung des WF osCreation für Preise und Ersatzartikel

Stand 11.06.2021:
WWS-4506 Performanceverbesserung durch guid, statt number
 
Stand 14.05.2021:
Erweiterung um originator

Stand 22.04.2021:
Teilstorno Anpassung/Erweiterung von customerNumber

Stand 12.02.2021:
Anpassungen von WWS-4386

Stand: 08.02.2021
- ORDER_INVOICED entfernt

*/


var beschreibung as String;
var url as String;
var inhalt as String;
var oql as String;
var param as Unknown[];
type Row as Unknown[];
var rs as Row[];
var i as Number;
var status as String;
var order as String;
var items as String;
var payments as String;
var shipment as String;
var flag as String;
var deposit as String;
var note as String;
var transactionid as String;
var credit as String;
var ende as String;
var k as Number;
var l as Number;
var date as String;
var timestamp as String;
var time as String;
var offset as String;
var hourGMT as Number;
var hourCET as Number;
var alt as String;
var quantity as String;
var today as String;
var parameter as String;
var tourNumber as String;
var lvsStatusOld as String;
var tourStatusOld as String;
var orderStatusOld as String;
var dayGMT as Number;
var dayCET as Number;
var monthGMT as Number;
var monthCET as Number;
var yearGMT as Number;
var yearCET as Number;
var dateGMT as CisDate;
var dateCET as CisDate;
var tGMT as Timestamp;
var tCET as Timestamp;
/* WWS-4386 Erweiterung    +2 Zeile  */
var deliveredQuantity as String;
var customerNumber as String;
/* Erweiterung um WWS-4451  +1 */
var originator as String;

function create()
{
/* set job queue and user for batch job */
setJobQueue("JQOT");
setJobUser("ADMINISTRATOR");

shipment:="";
status:=parameters.STATUS;
order:=parameters.ORDER;
tourNumber:=parameters.TOUR;
/* Erweiterung um WWS-4451  +2 */
originator:=" ";
originator:=parameters.ORIGINATOR;

/* CREATE URL:------------------------------------------------------------------ */

if (status = "ORDER_PROCESSING")
{
url := "https://order.connect-bm.de/api/erp/orders/" + order;
tourNumber := "";
}
else if (status = "ORDER_PENDING")
{
url := "https://order.connect-bm.de/api/erp/orders/" + order;
tourNumber := "";
}
else if (status = "SHIPMENT_CREATED")
{
url := "https://order.connect-bm.de/api/erp/orders/" + order + "/shipments/" + tourNumber;
shipment:=char(34) + "shipmentNumber" + char(34) + ":" + char(34) + tourNumber + char(34) + ",";
}
else if (status = "SHIPMENT_DISPATCHED")
{
url := "https://order.connect-bm.de/api/erp/orders/" + order + "/shipments/" + tourNumber;
shipment:=char(34) + "shipmentNumber" + char(34) + ":" + char(34) + tourNumber + char(34) + ",";
}
else if (status = "SHIPMENT_DELIVERED")
{
url := "https://order.connect-bm.de/api/erp/orders/" + order + "/shipments/" + tourNumber;
shipment:=char(34) + "shipmentNumber" + char(34) + ":" + char(34) + tourNumber + char(34) + ",";
}
else if (status = "ORDER_PAID")
{
url := "https://order.connect-bm.de/api/erp/orders/" + order;
}
else if (status = "ORDER_CANCELLED")
{
url := "https://order.connect-bm.de/api/erp/orders/" + order;
tourNumber := "";
}
else if (status = "SHIPMENT_CANCELLED")
{
url := "https://order.connect-bm.de/api/erp/orders/" + order + "/shipments/" + tourNumber;
shipment:=char(34) + "shipmentNumber" + char(34) + ":" + char(34) + tourNumber + char(34) + ",";
}
/*Teilstorno*/
else if (status = "PARTIAL_CANCELLATION_UPDATED")
{
url := "https://order.connect-bm.de/api/erp/orders/" + order + "/shipments/" + tourNumber;
shipment:=char(34) + "shipmentNumber" + char(34) + ":" + char(34) + tourNumber + char(34) + ",";
}

/* CREATE INHALT:------------------------------------------------------------ */

/* TIMESTAMP: */
today:= format(now(),"yyyyMMdd","de");
date:= format(now(),"yyyy-MM-dd","de");
dateGMT:=createCisDate("GMT",now());
dateCET:=createCisDate("CET",now());
hourGMT:=hour(dateGMT);
hourCET:=hour(dateCET);
dayGMT:=day(dateGMT);
dayCET:=day(dateCET);
monthGMT:=month(dateGMT);
monthCET:=month(dateCET);
yearGMT:=year(dateGMT);
yearCET:=year(dateCET);
tGMT:=createTimestamp(yearGMT, monthGMT, dayGMT, hourGMT, 0,0,0);
tCET:=createTimestamp(yearCET, monthCET, dayCET, hourCET, 0,0,0);
offset:=hours(tGMT,tCET);
time:= format(now(),"HH:mm:ss","de");
timestamp:=date + "T" + time + "+0" + offset + ":00";


/* OQL: */

oql:="SELECT SOD:number, IT:number, UOM:code, SOD:totalQuantity.amount, SOD:retailGrossPrice.amount, CU:isoCode, PM:code, SO:bring_transactionID, SOD:bring_positionType, SOD:canceled, "+
" SOD:bring_emptiesValue.amount, CU2:isoCode, SOD:bring_originalItemPos, IPU:bring_positionId, SOH1:description, SOD:bring_differentQuantity.amount, SO:bring_refundAmount.amount1, SOD:retailNetPrice.amount, SO:bring_remainingRefundAmount.amount1 "+
/* WWS-4386 Erweiterung    +1 Zeile  */ /* WWS-4516 Erweiterung umSOD:netDetailValue.amount  */
" , P:number,  SUM(SOD2:deliveredQuantity.amount), UOM2:code , SOD:netDetailValue.amount "+
/* SRV-17935  KG Artikel wurde in Stk bestellt und Kg ausgeliefert  +1 Zeile */
" , SUM(SOD2:deliveredQuantities[0].amount),  UOM3:code "+
" FROM com.cisag.app.sales.obj.SalesOrderDetail SOD " +
" JOIN com.cisag.app.general.obj.Item IT ON IT:guid=SOD:item "+
" JOIN com.cisag.app.general.obj.Currency CU ON CU:guid = SOD:grossPrice.currency "+
" LEFT JOIN com.cisag.app.general.obj.Currency CU2 ON CU2:guid = SOD:bring_emptiesValue.currency "+
" JOIN com.cisag.app.general.obj.UnitOfMeasure UOM ON UOM:guid = SOD:totalQuantity.uom "+
" JOIN com.cisag.app.sales.obj.SalesOrder SO ON SO:guid = SOD:header "+
/* CONNECT-3711 Erweiterung um Kundennummer  JOIN Partner */
" JOIN com.cisag.app.general.obj.Partner P ON SO:pricingCustomer=P:guid "+ 
" JOIN com.cisag.app.general.obj.InvoicingDataInfo IDI ON IDI:guid = SO:invoicingData "+
" JOIN com.cisag.app.general.obj.PaymentMethod PM ON PM:guid = IDI:paymentMethod "+
" LEFT JOIN com.cisag.app.general.obj.ItemPackagingUom IPU ON IPU:item = SOD:item AND IPU:sourceUom = SOD:totalQuantity.uom "+
" LEFT JOIN com.cisag.app.sales.obj.SalesOrderHierarchy1 SOH1 ON SOH1:guid = SO:classification1 "+
/* WWS-4386 Erweiterung    +2 Zeile  */
" JOIN com.cisag.app.sales.obj.SalesOrderDetail SOD2 ON SOD2:item = SOD:item AND SOD2:header = SOD:header "+
" JOIN com.cisag.app.general.obj.UnitOfMeasure UOM2 ON UOM2:guid = SOD:deliveredQuantity.uom "+
/* SRV-17935  KG Artikel wurde in Stk bestellt und Kg ausgeliefert  +1 Zeile */
" JOIN com.cisag.app.general.obj.UnitOfMeasure UOM3 ON UOM3:guid =SOD2:deliveredQuantities[0].uom "+
/* WWS-4506 Änderung    +1 Zeile  */
" WHERE SO:guid= ? AND SOD:totalQuantity.amount >= 0 "+
/* WWS-4386 Erweiterung    +3 Zeilen  */
" GROUP BY SOD:number, IT:number, UOM:code, SOD:totalQuantity.amount, SOD:retailGrossPrice.amount, CU:isoCode, PM:code, SO:bring_transactionID, SOD:bring_positionType, SOD:canceled, "+
" SOD:bring_emptiesValue.amount, CU2:isoCode, SOD:bring_originalItemPos, IPU:bring_positionId, SOH1:description, SOD:bring_differentQuantity.amount, SO:bring_refundAmount.amount1 "+
" ,SOD:retailNetPrice.amount, P:number, SO:bring_remainingRefundAmount.amount1, UOM2:code, SO:bring_tourStatus, SOD:netDetailValue.amount  "+    /* WWS-4516 Erweiterung umSOD:netDetailValue.amount  */
/* SRV-17935  KG Artikel wurde in Stk bestellt und Kg ausgeliefert  +1 Zeile */
" , UOM3:code "+
" ORDER BY SOD:number ASC";

/* WWS-4506 Änderung    +2 Zeilen  */
param:=new(Guid[1]);
param[0]:=cast(Guid, parameters.GUID);
rs:=getResultList(oql, param, 999);

i:=0;
/* WWS-4386 Erweiterung   +3 Zeile  */
deliveredQuantity:="0";
customerNumber:="0";
customerNumber:=cast(String, rs[i][19]);

     while (i<size(rs)) {

flag:="";
deposit:="";
note:="";
transactionid:="";
credit:="";
ende:="},";
k:=i + 1;

if (k = size(rs))
{
ende:="}";
}


/* FLAG: */

if (cast(String, rs[i][1]) = "Z00001" OR cast(String, rs[i][1]) = "Z00011")
{
flag:="," + char(34) + "flags" + char(34) + ":[{" + char(34) + "type" + char(34) + ":" + char(34) + "DELIVERY_FEE" + char(34) + "}]";
}
else if (cast(String, rs[i][1]) = "C00001")
{
flag:="," + char(34) + "flags" + char(34) + ":[{" + char(34) + "type" + char(34) + ":" + char(34) + "DISCOUNT_COUPON" + char(34) + "}]";
}
else if (cast(String, rs[i][8]) = "6.004")
{
flag:="," + char(34) + "flags" + char(34) + ":[{" + char(34) + "type" + char(34) + ":" + char(34) + "ADDITIONAL_ITEM" + char(34) + "}]";
}
else if (cast(Number, rs[i][0]) >= 5000)
{
flag:="," + char(34) + "flags" + char(34) + ":[{" + char(34) + "type" + char(34) + ":" + char(34) + "REPLACEMENT_ITEM" + char(34) + "," + char(34) + "referencePositionId" + char(34) + ":" + cast(String, rs[i][12]) + "}]";
}
else if (cast(String, rs[i][9]) = "true")
{
if (cast(String, rs[i][14]) <> "")
{
note:="," + char(34) + "note" + char(34) + ":" + char(34) + cast(String, rs[i][14]) + char(34);
}
else if (cast(Number, rs[i][15]) = cast(Number, rs[i][3]))
{
note:="," + char(34) + "note" + char(34) + ":" + char(34) + "Out of Stock" + char(34);
}
flag:="," + char(34) + "flags" + char(34) + ":[{" + char(34) + "type" + char(34) + ":" + char(34) + "CANCELLED_ITEM" + char(34) + note + "}]";
}
else if (cast(String, rs[i][1]) = "LEERGUT")
{
flag:="," + char(34) + "flags" + char(34) + ":[{" + char(34) + "type" + char(34) + ":" + char(34) + "DEPOSIT_RETURN" + char(34) + "}]";
}


/* DEPOSIT: */

if (cast(Number, rs[i][10]) > 0)
{
deposit:="," + char(34) + "deposit" + char(34) + ":{" + char(34) + "amount" + char(34) + ":" + format(cast(Number, rs[i][10]) * cast(Number, rs[i][20]),"###.##") + "," + char(34) + "currency" + char(34) + ":" + char(34) + cast(String, rs[i][11]) + char(34) + "}";
}

/* QUANTITY DELIVERY FEE: */

if (cast(String, rs[i][1]) = "Z00011")
{
quantity:="1";
/* WWS-4386 Erweiterung    +2 Zeile  */
if (status="SHIPMENT_DISPATCHED" or status = "SHIPMENT_DELIVERED" OR status = "PARTIAL_CANCELLATION_UPDATED"){
deliveredQuantity :="1"; }
}
else
{
quantity:=format(cast(Number, rs[i][3]), "###.##");
/* WWS-4386 Erweiterung    +2 Zeile  */
if (status="SHIPMENT_DISPATCHED" OR status = "SHIPMENT_DELIVERED" OR status = "PARTIAL_CANCELLATION_UPDATED"){
	/* SRV-17935  KG Artikel wurde in Stk bestellt und Kg ausgeliefert  +1 Zeile */
if (cast(String, rs[i][21]) = "Stk" AND cast(String, rs[i][24]) = "KG")  { deliveredQuantity :=format(cast(Number, rs[i][23]), "###.###");}
else{
deliveredQuantity :=format(cast(Number, rs[i][20]), "###.###"); }
}
}
/* ITEMS: */
/* WWS-4516 Anpassung für Ersatzartikel BEGINN */
var positionId:=cast(String, rs[i][0]);
if(indexOf(positionId, ".")>0){ 
positionId:=format(cast(Number, rs[i][0]), "######");
}  /* WWS-4516 Anpassung für Ersatzartikel ENDE*/

/* WWS-4386 Erweiterung    + Block ITEMS  für SHIPMENT_DISPATCHED und SHIPMENT_DELIVERED separat gesendet */
if (status="SHIPMENT_DISPATCHED" or  status = "SHIPMENT_DELIVERED" OR status = "PARTIAL_CANCELLATION_UPDATED" ){
/* BEGINN  SRV-17909 Anpassung der Übergabe von deliveredUnit  */
var deliveredUnit as String;
if (cast(String, rs[i][21]) = "KG")  { deliveredUnit:="Kg"; }
else if (cast(String, rs[i][21]) = "ST") { deliveredUnit:="Stk"; }
/* SRV-17935  KG Artikel wurde in Stk bestellt und Kg ausgeliefert  +1 Zeile */
else if (cast(String, rs[i][21]) = "Stk" AND cast(String, rs[i][24]) = "KG")  { deliveredUnit:="Kg"; }
else { deliveredUnit:=cast(String, rs[i][21]); }
/* ENDE SRV-17909 Anpassung der Übergabe von deliveredUnit  */

items:= items + "{" + char(34) + "positionId" + char(34) + ":" + positionId+ /* WWS-4516 Anpassung für Ersatzartikel positionId  */
"," + char(34) + "articleNumber" + char(34) + ":" + char(34) + cast(String, rs[i][1]) + cast(String, rs[i][13]) + char(34) + 
"," + char(34) + "articleBaseNumber" + char(34) + ":" + char(34) + cast(String, rs[i][1]) + char(34) +
/* WWS-4386 Erweiterung  Einheit wird mit der Liefereinheit vom rs[i][20] befüllt */
"," + char(34) + "unit" + char(34) + ":" + char(34) + cast(String, rs[i][2]) + char(34) +
"," + char(34) + "quantity" + char(34) + ":" + quantity +
"," + char(34) + "deliveredQuantity" + char(34) + ":" + deliveredQuantity + 
"," + char(34) + "deliveredUnit" + char(34) + ":" + char(34) +  deliveredUnit + char(34) +  /*  <= SRV-17909 Anpassung:  cast(String, rs[i][21]) wurde durch deliveredUnit ersetzt */
 /* WWS-4516  Erweiterung um grossAmount  +1 Zeile  und Anpassung grossAmount=deliveredQuantity * grossDiscountedPrice */
"," + char(34) + "grossAmount" + char(34) + ":{" + char(34) + "amount" + char(34) + ":" +   format(cast(Number, rs[i][20]) * cast(Number, rs[i][17]),"###.##") + 
"," + char(34) + "currency" + char(34) + ":" + char(34) + cast(String, rs[i][5]) + char(34) + "}" +
"," + char(34) + "grossRetailPrice" + char(34) + ":{" + char(34) + "amount" + char(34) + ":" +  format(cast(Number, rs[i][4]),"###.##") +
"," + char(34) + "currency" + char(34) + ":" + char(34) + cast(String, rs[i][5]) + char(34) + "}" +
"," + char(34) + "grossDiscountedPrice" + char(34) + ":{" + char(34) + "amount" + char(34) + ":" +  format(cast(Number, rs[i][17]),"###.##") +
"," + char(34) + "currency" + char(34) + ":" + char(34) + cast(String, rs[i][5]) + char(34) + "}" + flag + deposit + ende;


} else 
{
items:= items + "{" + char(34) + "positionId" + char(34) + ":" + cast(String, rs[i][0]) +
"," + char(34) + "articleNumber" + char(34) + ":" + char(34) + cast(String, rs[i][1]) + cast(String, rs[i][13]) + char(34) + 
"," + char(34) + "articleBaseNumber" + char(34) + ":" + char(34) + cast(String, rs[i][1]) + char(34) +
"," + char(34) + "unit" + char(34) + ":" + char(34) + cast(String, rs[i][2]) + char(34) +
"," + char(34) + "quantity" + char(34) + ":" + quantity +
"," + char(34) + "grossRetailPrice" + char(34) + ":{" + char(34) + "amount" + char(34) + ":" +  format(cast(Number, rs[i][4]),"###.##") +
"," + char(34) + "currency" + char(34) + ":" + char(34) + cast(String, rs[i][5]) + char(34) + "}" +
"," + char(34) + "grossDiscountedPrice" + char(34) + ":{" + char(34) + "amount" + char(34) + ":" +  format(cast(Number, rs[i][17]),"###.##") +
"," + char(34) + "currency" + char(34) + ":" + char(34) + cast(String, rs[i][5]) + char(34) + "}" + flag + deposit + ende;
}            
i:=i+1;


}

if (cast(String, rs[0][7]) <> "")
{
transactionid:="," + char(34) + "transactionId" + char(34) + ":" + char(34) + cast(String, rs[0][7]) + char(34);
}

/* CREDIT: */
if (cast(Number, rs[0][16]) > 0)
{
if (cast(Number, rs[0][18]) > 0 AND status = "ORDER_PAID")
{
credit:=",{" + char(34) + "type" + char(34) + ":" + char(34) + "bringmeister/credit-account" + char(34) + "," + char(34) + "grossAmount" + char(34) + ":{" + char(34) + "amount" + char(34) + ":" + format(cast(Number, rs[0][16]), "###.##") + "," + 
char(34) + "currency" + char(34) + ":" +  char(34) + cast(String, rs[0][5]) + char(34) + "}}" + ",{" + char(34) + "type" + char(34) + ":" + char(34) + "bringmeister/credit-account" + char(34) + "," + char(34) + "grossAmount" + char(34) + 
":{" + char(34) + "amount" + char(34) + ":" + format((cast(Number, rs[0][18])*-1), "###.##") + "," + char(34) + "currency" + char(34) + ":" +  char(34) + cast(String, rs[0][5]) + char(34) + "}}";
}
else
{
credit:=",{" + char(34) + "type" + char(34) + ":" + char(34) + "bringmeister/credit-account" + char(34) + "," + char(34) + "grossAmount" + char(34) + ":{" + char(34) + "amount" + char(34) + ":" + format(cast(Number, rs[0][16]), "###.##") + "," + 
char(34) + "currency" + char(34) + ":" +  char(34) + cast(String, rs[0][5]) + char(34) + "}}";
}
}

payments:= "{" + char(34) + "type" + char(34) + ":" + char(34) + cast(String, rs[0][6]) + char(34) + transactionid + "}";

/* SUMMARY: */          /* Erweiterung von SUMMARY um IF-ELSE-Block für WWS-4451  */
if (status = "PARTIAL_CANCELLATION_UPDATED"){
inhalt := "{" + char(34) + "occurredAt" + char(34) + ":" + char(34) + timestamp + char(34) +
"," + char(34) + "orderNumber" + char(34) + ":" + char(34) + order + char(34) +
"," + shipment + char(34) + "status" + char(34) + ":" + char(34) + status + char(34) + 
/* CONNECT-3711 Erweiterung um Kundennummer */
"," + char(34) + "customerNumber" + char(34) + ":" + char(34) + customerNumber+ char(34) + 
/* Erweiterung um WWS-4451  +1 */
 "," + char(34) + "originator" + char(34) + ":" + char(34) + originator+ char(34) + 

"," + char(34) + "items" + char(34) + ":[" 
+ items + "]," + char(34) + "payments" + char(34) + ":[" + payments + credit + "]}";
} 
else {
inhalt := "{" + char(34) + "occurredAt" + char(34) + ":" + char(34) + timestamp + char(34) +
"," + char(34) + "orderNumber" + char(34) + ":" + char(34) + order + char(34) +
"," + shipment + char(34) + "status" + char(34) + ":" + char(34) + status + char(34) + 
/* CONNECT-3711 Erweiterung um Kundennummer */
"," + char(34) + "customerNumber" + char(34) + ":" + char(34) + customerNumber+ char(34) + 
"," + char(34) + "items" + char(34) + ":[" 
+ items + "]," + char(34) + "payments" + char(34) + ":[" + payments + credit + "]}";
}

/* CREATE BESCHREIBUNG:--------------------------------------------------------- */
beschreibung := order +" " + status + " " + tourNumber;


/* CREATE RESULT:--------------------------------------------------------- */
result.BESCHREIBUNG:=beschreibung;
result.URL:=url;
result.INHALT:=inhalt;
}

