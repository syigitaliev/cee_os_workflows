/*
Stand 11.06.2021:
WWS-4506 Performanceverbesserung

Stand: 15.01.2021

*/

/* WWS-4506 Erweiterung    +1 Zeile  */
var orderGuid as Guid;
var order as String;
var status as String;
var orderType as String;
var orderStatus as String;
var tourNumberOld as String;
var tourNumberNew as String;
var tourOld as CisObject(com.bring.app.sales.obj.Tour);
var tourNew as CisObject(com.bring.app.sales.obj.Tour);

function create()
{
/* set job queue and user for batch job */
setJobQueue("JQOT");
setJobUser("ADMINISTRATOR");

/* WWS-4506 Erweiterung    +1 Zeile  */
orderGuid:=parameters.object:guid;
order:=parameters.object:number;
status:="SHIPMENT_CANCELLED";
tourOld:= parameters.oldObject->Bring_Tour;
tourNew:= parameters.newObject->Bring_Tour;

if (isNull(tourOld)) 
{
abort();
}
else 
{
tourNumberOld:=tourOld:number;
}

if (isNull(tourNew)) 
{
tourNumberNew:="";
}
else 
{
tourNumberNew:=tourNew:number;
}

/*Ignore wrong ordertypes or multiple events ---------------*/

orderType:=parameters.newObject->Type:code;
orderStatus:=status + " " + order + " " + tourNumberOld;

if (cast(Number, getResultList("SELECT count(*) FROM com.cisag.sys.workflow.obj.Activity A WHERE A:description = ?", list(orderStatus))[0][0]) > 0 OR (orderType <> "105" AND orderType <> "104") OR startsWith(order, "VA") = true)
{
abort();
}


/*Call osCheck:---------------*/

formatSubject("Beschreibung", orderStatus);
/* WWS-4506 Anpassung    +1 Zeile  */
call("osCheck", hashMap(ORDER:=order, STATUS:=status, MODUS:="complete", TOUR:=tourNumberOld, GUID:=orderGuid));
/* WWS-4506 Anpassung    +1 Zeile  */
call("osCheck", hashMap(ORDER:=order, STATUS:="SHIPMENT_CREATED", MODUS:="single", TOUR:=tourNumberNew, GUID:=orderGuid));
}
