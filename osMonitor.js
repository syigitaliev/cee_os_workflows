/*Stand: 23.04.2021*/

var order as String;
var oldOrder as String;
var orderGuid as Guid;
var status as String;
var tourNumber as String;
var today as String;
var hour as String;
var oql as String;
var i as Number;
var tourStatus as String;
var cancel as String;
var rogNumber as String;
var rogNumbers as String;
type Row as Unknown[];
var rs as Row[];
var paramOql as Unknown[];

function create()
{
/* set job queue and user for batch job */
setJobQueue("JQOT");
setJobUser("ADMINISTRATOR");
today:= format(now(),"yyyyMMddHHmm","de");
hour:=substring(today,8,10);
oldOrder:="";
echo("Starte OS-Monitor");

if (hour <> "00" AND hour <> "04" AND hour <> "08" AND hour <> "12" AND hour <> "16" AND hour <> "20")
{
abort();
}

oql:=" SELECT DISTINCT SO:number, SO:bring_tourStatus, SO:canceled, T:number, ROG:number, SO:guid "+
" FROM com.cisag.app.sales.obj.SalesOrder SO "+
" JOIN com.cisag.app.sales.obj.SalesOrderType SOT On SO:type = SOT:guid "+
" LEFT JOIN com.bring.app.sales.obj.Tour T ON T:guid = SO:bring_tour "+
" LEFT JOIN com.cisag.app.purchasing.obj.ReceiptOfGoodsOrderInfo ROGI ON ROGI:\"order\" = SO:guid "+
" LEFT JOIN com.cisag.app.purchasing.obj.ReceiptOfGoods ROG ON ROG:guid = ROGI:header AND ROG:updateInfo.updateTime >= (SYSTEMTIME + toTimeStamp('CET', 2018, 2, 3, 8, 0,0,0) - toTimeStamp('CET', 2018, 2, 3, 12, 0,0,0)) AND ROG:status = 3 "+
" LEFT JOIN com.cisag.app.purchasing.obj.ReceiptOfGoodsType ROGT ON ROGT:guid = ROG:type AND ROGT:code IN ('404','405') "+
" WHERE SOT:code IN ('104', '105') AND SO:updateInfo.updateTime >= (SYSTEMTIME + toTimeStamp('CET', 2018, 2, 3, 8, 0,0,0) - toTimeStamp('CET', 2018, 2, 3, 12, 0,0,0)) "+
" ORDER BY SO:number ";

rs:=getResultList(oql, paramOql, 10000);

i:=0;

while(i<size(rs))
{
order:=cast(String, rs[i][0]);
tourStatus:=cast(String, rs[i][1]);
cancel:=cast(String, rs[i][2]);

if (isNull(cast(String, rs[i][3]))) 
{
tourNumber := "";
}
else 
{
tourNumber:=cast(String, rs[i][3]);
}

if (isNull(cast(String, rs[i][4]))) 
{
rogNumber:= "";
}
else 
{
rogNumber:= cast(String, rs[i][4]);
}

if(cancel = "true") /*stornierte Aufträge überprüfen*/
{
call("osCheck", hashMap(ORDER:=order, STATUS:="ORDER_CANCELLED", MODUS:="single", TOUR:=""));
}
else if(tourStatus = "6.011" OR tourStatus = "6.009" OR tourStatus = "6.010")
{

if (rogNumber <> "")
{
orderGuid:=cast(Guid, rs[i][5]);
var getRogs:=call("getRogs", hashMap(ORDER:=orderGuid));
rogNumbers:=cast(String, getRogs.ROGS);

if (length(rogNumbers) > 11)
{
rogNumbers:=substring(rogNumbers, 0, indexOf(rogNumbers, rogNumber)) + rogNumber;
}

call("osCheck", hashMap(ORDER:=order, STATUS:="PARTIAL_CANCELLATION_UPDATED", MODUS:="single", TOUR:=tourNumber, ROGS:=rogNumbers));
}

if(tourStatus = "6.011" AND order <> oldOrder) /*abgerechnete Aufträge überprüfen*/
{
call("osCheck", hashMap(ORDER:=order, STATUS:="ORDER_PAID", MODUS:="single", TOUR:=tourNumber));
oldOrder:=order;
}

}

i:=i + 1;
}

}

