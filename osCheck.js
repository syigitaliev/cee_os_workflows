/*
Stand 11.06.2021
WWS-4506 Performanceverbesserung durch guid, statt number

Stand: 22.04.2021
Order-Status PARTIAL_CANCELLATION für Teilstorno einführen

Stand: 25.03.2021
Anpassung WWS-4412
- durch Variable activityState überprüfen, ob Status der Aktivität < "erledigt" ist und in diesem Fall keinen erneuten Versand anstoßen

*/

var modus as String;
var checkOrder as String;
var checkStatus as String;
var checkTour as String;
var statusChecks as String[];
var faultyChecks as String;
var oqlStatusChecks as String;
type Row as Unknown[];
var rsStatusChecks as Row[];
var paramOqlStatusChecks as Unknown[];
var i as Number;
var k as Number;
var oqlStatusEntry as String;
var statusCheckEntry as String;
var mleEntry as String;
var timeCheck as String;
var vor_oql as Timestamp;
var nach_oql as Timestamp;
var laufzeit_oql as Number;
var errorCount as Number[];
var url as String;
var beschreibung as String;
var inhalt as String;
var start as Number;
var ende as Number;
var tourCheck as Boolean;
var activityState as Number;
/*Teilstorno*/
var rogCheck as Boolean;
var checkRogs as String;
/* Erweiterung um WWS-4451  +1 */
var originator as String;
/* Erweiterung um WWS-4506  +1 */
var orderGuid as String;

function create()
{
/* set job queue and user for batch job */
setJobQueue("JQOT");
setJobUser("ADMINISTRATOR");

modus:=parameters.MODUS;
checkOrder:=parameters.ORDER;
checkStatus:=parameters.STATUS;
checkTour:=parameters.TOUR;
/*Teilstorno*/
checkRogs:=parameters.ROGS;
/* Erweiterung um WWS-4506  +1 */
orderGuid:=parameters.GUID;

statusChecks:=new(String[6]);
errorCount:=new(Number[6]);
faultyChecks:=""; 

/* Erweiterung um WWS-4451  +1 */
originator:=parameters.ORIGINATOR;

if (isNull(checkTour)) 
{
checkTour:= "";
}

/*Teilstorno*/
if (isNull(checkRogs)) 
{
checkRogs:= "";
}

if(modus = "complete")
{

/*Vorherige Statuszustände für die Überprüfung ermitteln*/


if(checkStatus = "ORDER_PENDING")
{
statusChecks[0]:="ORDER_PENDING";
} 
else if(checkStatus = "ORDER_CANCELLED")
{
statusChecks[0]:="ORDER_PENDING";
statusChecks[1]:="ORDER_CANCELLED";
} 
else if(checkStatus = "ORDER_PROCESSING")
{
statusChecks[0]:="ORDER_PENDING";
statusChecks[1]:="ORDER_PROCESSING";
}
else if(checkStatus = "SHIPMENT_CREATED")
{
statusChecks[0]:="ORDER_PENDING";
statusChecks[1]:="ORDER_PROCESSING";
statusChecks[2]:="SHIPMENT_CREATED";
}
else if(checkStatus = "SHIPMENT_CANCELLED")
{
statusChecks[0]:="ORDER_PENDING";
statusChecks[1]:="ORDER_PROCESSING";
statusChecks[2]:="SHIPMENT_CANCELLED";
statusChecks[3]:="SHIPMENT_CREATED";
}
else if(checkStatus = "SHIPMENT_DISPATCHED")
{
statusChecks[0]:="ORDER_PENDING";
statusChecks[1]:="ORDER_PROCESSING";
statusChecks[2]:="SHIPMENT_CREATED";
statusChecks[3]:="SHIPMENT_DISPATCHED";
}
else if(checkStatus = "SHIPMENT_DELIVERED")
{
statusChecks[0]:="ORDER_PENDING";
statusChecks[1]:="ORDER_PROCESSING";
statusChecks[2]:="SHIPMENT_CREATED";
statusChecks[3]:="SHIPMENT_DISPATCHED";
statusChecks[4]:="SHIPMENT_DELIVERED";
}
/*Teilstorno: Entfernung des Event SHIPMENT_DELIVERED für WWS-4469 */ 
else if(checkStatus = "PARTIAL_CANCELLATION_UPDATED")
{
statusChecks[0]:="ORDER_PENDING";
statusChecks[1]:="ORDER_PROCESSING";
statusChecks[2]:="SHIPMENT_CREATED";
statusChecks[3]:="SHIPMENT_DISPATCHED";
statusChecks[4]:="PARTIAL_CANCELLATION_UPDATED";
}
else if(checkStatus = "ORDER_PAID")
{
statusChecks[0]:="ORDER_PENDING";
statusChecks[1]:="ORDER_PROCESSING";
statusChecks[2]:="SHIPMENT_CREATED";
statusChecks[3]:="SHIPMENT_DISPATCHED";
statusChecks[4]:="SHIPMENT_DELIVERED";
statusChecks[5]:="ORDER_PAID";
}

/*Ermittle alle Aktivitäten, die sich auf die zu prüfende Order beziehen*/

oqlStatusChecks:=
" SELECT A:description, MLE:parameters[1], A2:description, A:state "+
" FROM com.cisag.sys.workflow.obj.Activity A "+
" LEFT JOIN com.cisag.sys.kernel.obj.MessageLogEntry MLE ON MLE:parameters[0] = A:description AND MLE:parameters[1] = '200' "+ /*OUTDATED: TESTANPASSUNG PREFIX MELDUNGSPROTOKOLLE */
" LEFT JOIN com.cisag.sys.workflow.obj.Activity A2 ON A2:guid = A:guid AND A:updateInfo.createTime >= (SYSTEMTIME + (toTimeStamp('CET', 2018, 4, 16, 4, 0,0,0) - toTimeStamp('CET', 2018, 4, 30, 4, 0,0,0))) "+
" WHERE A:description LIKE ?";

}
else
{

/*Status der überprüft werden soll*/
statusChecks[0]:=checkStatus;

/*Ermittle alle Aktivitäten, die sich auf die zu prüfende Order beziehen und nicht älter als 1,5h sind*/

oqlStatusChecks:=
" SELECT A:description, MLE:parameters[1], A2:description, A:state "+
" FROM com.cisag.sys.workflow.obj.Activity A "+
" LEFT JOIN com.cisag.sys.kernel.obj.MessageLogEntry MLE ON MLE:parameters[0] = A:description AND MLE:parameters[1] = '200' "+ /*OUTDATED: TESTANPASSUNG PREFIX MELDUNGSPROTOKOLLE */
" LEFT JOIN com.cisag.sys.workflow.obj.Activity A2 ON A2:guid = A:guid AND A:updateInfo.createTime >= (SYSTEMTIME + (toTimeStamp('CET', 2018, 4, 16, 4, 0,0,0) - toTimeStamp('CET', 2018, 4, 30, 4, 0,0,0))) "+
" AND A:updateInfo.createTime >= (SYSTEMTIME + toTimeStamp('CET', 2018, 2, 3, 2, 30,0,0) - toTimeStamp('CET', 2018, 2, 3, 4, 0,0,0)) WHERE A:description LIKE ?";

}

if (modus <> "manualStatus")
{
paramOqlStatusChecks:=new(Unknown[1]);
paramOqlStatusChecks[0]:=checkOrder + "%"; /*OUTDATED: Testmodus für Parallelbetrieb mit altem Order-Status durch "Test"-Prefix*/

/*vor_oql:=now();*/
rsStatusChecks:=getResultList(oqlStatusChecks, paramOqlStatusChecks, 1000);

  
/*nach_oql:=now();
laufzeit_oql:=milliseconds(vor_oql, nach_oql)/1000;*/

i:=0;

while(i<size(rsStatusChecks)) 
{
oqlStatusEntry:=cast(String, rsStatusChecks[i][0]);
activityState:=cast(Number, rsStatusChecks[i][3]);

if (isNull(cast(String, rsStatusChecks[i][1]))) 
{
mleEntry:= "";
}
else
{
mleEntry:=cast(String, rsStatusChecks[i][1]);
}

if (isNull(cast(String, rsStatusChecks[i][2]))) 
{
timeCheck:= "";
}
else
{
timeCheck:=cast(String, rsStatusChecks[i][2]);
}

k:=0;

while(k<size(statusChecks))
{
statusCheckEntry:=cast(String, statusChecks[k]);

if(statusCheckEntry = "ORDER_PENDING" OR statusCheckEntry  = "ORDER_PROCESSING" OR statusCheckEntry = "ORDER_CANCELLED")
{
tourCheck:= false;
/*Teilstorno*/
rogCheck:=false;
}
else
{
tourCheck:=true;
/*Teilstorno*/
if (statusCheckEntry = "PARTIAL_CANCELLATION_UPDATED")
{
rogCheck:=true;
}
else
{
rogCheck:=false;
}
}
if (statusCheckEntry <> "" AND ((tourCheck = true AND rogCheck = false AND indexOf(oqlStatusEntry, statusCheckEntry + " " + checkTour) > 0) OR (tourCheck = true AND rogCheck = false AND indexOf(oqlStatusEntry, statusCheckEntry + checkTour) > 0) OR (tourCheck = false AND indexOf(oqlStatusEntry, statusCheckEntry) > 0)
/*Teilstorno*/ 
OR (rogCheck = true AND indexOf(oqlStatusEntry, statusCheckEntry + " " + checkTour + " " + checkRogs) > 0) OR (rogCheck = true AND indexOf(oqlStatusEntry, statusCheckEntry + checkTour + " " + checkRogs) > 0)))
{
errorCount[k]:=errorCount[k] + 1;
/*echo("Zähler: " + errorCount[k] + " und status: " + statusCheckEntry + " Order: " + checkOrder);*/

if((oqlStatusEntry = timeCheck AND (mleEntry = "200" OR activityState < 40)) OR (timeCheck = ""))
{
statusChecks[k]:="";
}
else if (oqlStatusEntry = timeCheck AND errorCount[k] = 2)
{
/*Berichte fehlerhaften Statuszustand: Übertragung nicht erfolgreich*/
echo("Erstelle Fehlerbericht für: " + checkOrder + " " + statusCheckEntry);
/*Teilstorno*/
if (statusCheckEntry = "PARTIAL_CANCELLATION_UPDATED")
{
fireEvent("faultyOrderStatus", hashMap(FAULTYORDERSTATUS:=(checkOrder + " " + statusCheckEntry + " " + checkTour + " " + checkRogs)));
}
else
{
fireEvent("faultyOrderStatus", hashMap(FAULTYORDERSTATUS:=(checkOrder + " " + statusCheckEntry + " " + checkTour)));
}
statusChecks[k]:="";
} 

} 
k:=k + 1;
}
i:=i + 1;
}

}

/*Erzeuge und übertrage fehlende Statuszustände*/
i:=0;
while(i<size(statusChecks)) 
{
if(statusChecks[i] <> "")
{
statusCheckEntry:=cast(String, statusChecks[i]);
url:="";
inhalt:="";
beschreibung:="";

/*Änderung WWS-4506*/
var osCreate:=call("osCreation", hashMap(ORDER:=checkOrder, STATUS:=statusCheckEntry, TOUR:=checkTour, ORIGINATOR:=originator, GUID:=orderGuid));

if (isNull(osCreate) OR cast(String, osCreate.INHALT) = "")
{
/*Berichte fehlerhaften Statuszustand: Statuserzeugung nicht erfolgreich*/
echo("Kein Inhalt");
echo("Erstelle Fehlerbericht für: " + checkOrder + " " + statusCheckEntry);
/*Teilstorno*/
if (statusCheckEntry = "PARTIAL_CANCELLATION_UPDATED")
{
fireEvent("faultyOrderStatus", hashMap(FAULTYORDERSTATUS:=(checkOrder + " " + statusCheckEntry + " " + checkTour + " " + checkRogs)));
}
else
{
fireEvent("faultyOrderStatus", hashMap(FAULTYORDERSTATUS:=(checkOrder + " " + statusCheckEntry + " " + checkTour)));
}


} 
else
{
url:=cast(String, osCreate.URL);

/*Teilstorno*/
if (statusCheckEntry = "PARTIAL_CANCELLATION_UPDATED")
{
beschreibung:=cast(String, osCreate.BESCHREIBUNG) + " " + checkRogs;
}
else
{
beschreibung:=cast(String, osCreate.BESCHREIBUNG);
}
inhalt:=cast(String, osCreate.INHALT);
fireEvent("transferOrderStatus", hashMap(URL:=url, INHALT:=inhalt, BESCHREIBUNG:=beschreibung));
}
}
i:=i + 1;
}

}

