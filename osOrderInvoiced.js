/*Stand: 15.01.2021*/

var order as String;
var status as String;
var orderType as String;
var orderStatus as String;
var tourNumber as String;
var tour as CisObject(com.bring.app.sales.obj.Tour);

function create()
{
/* set job queue and user for batch job */
setJobQueue("JQOT");
setJobUser("ADMINISTRATOR");

order:=parameters.object:number;
status:="ORDER_INVOICED";
tour:= parameters.newObject->Bring_Tour;

if (isNull(tour)) 
{
    tourNumber := "";
}
else 
{
    tourNumber:=tour:number;
}



/*Ignore wrong ordertypes or multiple events ---------------*/

orderType:=parameters.newObject->Type:code;
orderStatus:=status + " " + order + " " + tourNumber;

if (cast(Number, getResultList("SELECT count(*) FROM com.cisag.sys.workflow.obj.Activity A WHERE A:description = ?", list(orderStatus))[0][0]) > 0 OR (orderType <> "105" AND orderType <> "104") OR startsWith(order, "VA") = true)
{
abort();
}


/*Call osCheck:---------------*/

formatSubject("Beschreibung", orderStatus);
call("osCheck", hashMap(ORDER:=order, STATUS:=status, MODUS:="complete", TOUR:=tourNumber));
}
