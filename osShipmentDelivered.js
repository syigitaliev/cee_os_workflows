/*
Stand: 06.02.2023
- Anpassung für Pünktlichkeitsgarantie aus CEE-125

Stand: 11.03.2021
- Anpassung für zeitversetzte Ausführung aufgrund von WWS-4386
*/

var orderGuid as Guid;
var order as String;
var status as String;
var orderType as String;
var orderStatus as String;
var tourNumber as String;
var tour as CisObject(com.bring.app.sales.obj.Tour);
var windowFrom as Timestamp;
var windowTo as Timestamp;
var buffer as Number;
var customer as String;

function create()
{
/* set job queue and user for batch job */
setJobQueue("JQOT");
setJobUser("ADMINISTRATOR");

orderGuid:=parameters.object:guid;
order:=parameters.object:number;
status:="SHIPMENT_DELIVERED";
tour:= parameters.newObject->Bring_Tour;
windowFrom:=parameters.object:bring_deliveryTimeWindowFrom;
windowTo:=parameters.object:bring_deliveryTimeWindowTo;
customer:=getByPrimaryKey(CisObject(com.cisag.app.general.obj.Partner), parameters.object:pricingCustomer):number;
buffer:=19;

if (isNull(tour)) 
{
    tourNumber := "";
}
else 
{
    tourNumber:=tour:number;
}



/*Ignore wrong ordertypes or multiple events ---------------*/

orderType:=parameters.newObject->Type:code;
orderStatus:=status + " " + order + " " + tourNumber;

if (cast(Number, getResultList("SELECT count(*) FROM com.cisag.sys.workflow.obj.Activity A WHERE A:description = ?", list(orderStatus))[0][0]) > 0 OR (orderType <> "105" AND orderType <> "104") OR startsWith(order, "VA") = true)
{
abort();
}

if(minutes(now(), addMinutes(windowTo, buffer)) < 0)
{
setActivityResult("Delayed: " + order + ";" + format(windowFrom, "dd.MM.yyyy") + " " + format(windowFrom, "HH:mm") + " - " + format(windowTo, "HH:mm") + ";" + format(now(), "dd.MM.yyyy HH:mm") + ";" + customer);  
  
}
  
formatSubject("Beschreibung", orderStatus);

}

function close(state as Number)
{
/*Call osCheck:---------------*/
call("osCheck", hashMap(ORDER:=order, STATUS:=status, MODUS:="complete", TOUR:=tourNumber, GUID:=orderGuid));
}


