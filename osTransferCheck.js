/*
Stand: 27.04.2021 
- Anpassung für Teilstorno
*/

var orderStatus as String;
var order as String;
var status as String;
var tourNumber as String;
var rogNumbers as String;

function create()
{
orderStatus:=cast(String, parameters.parameters.ORDERSTATUS);
/*echo("TransferCheck: " + orderStatus);*/
order:=substring(orderStatus, 0, 10);
orderStatus:=substring(orderStatus, 11);
status:=substring(orderStatus, 0, indexOf(orderStatus, " "));
tourNumber:=substring(orderStatus, indexOf(orderStatus, " ")+1, indexOf(orderStatus, " ") + 11);
rogNumbers:=substring(orderStatus, indexOf(orderStatus, " ") + 12, length(orderStatus));
orderStatus:=order + " " + status;
formatSubject("Beschreibung", orderStatus + " " + tourNumber + " " + rogNumbers);
orderStatus:="TransferCheck für " + orderStatus;

}

function close(state as Number)
{
/*echo("Prüfaufruf für: " + order + " " + status + " " + tourNumber + " " + rogNumbers);*/
call("osCheck", hashMap(ORDER:=order, STATUS:=status, MODUS:="single", TOUR:=tourNumber, ROGS:=rogNumbers));
}



