/*Stand: 22.03.2021*/

var order as String;
var status as String;
var faultyOrderStatus as String;

function create() {

setJobQueue("JQBAT");

faultyOrderStatus:=cast(String, parameters.parameters.FAULTYORDERSTATUS);
order:=substring(faultyOrderStatus, 0, 10);
status:=substring(faultyOrderStatus, 11);
faultyOrderStatus:="Fehlerhafter Order-Status: " + order + " " + status;

if (cast(Number, getResultList("SELECT count(*) FROM com.cisag.sys.workflow.obj.Activity A WHERE A:description = ?", list(faultyOrderStatus))[0][0]) > 0)
{
abort();
}

formatDescription("Order", order);
formatDescription("Status", status);
formatSubject("Order", order);
formatSubject("Status", status);
}